<?php 
/*
* 2015 LocalAddict
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Benoit MOTTIN <benaesan@msn.com>
*  @copyright  LocalAddict
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

/**
 * Met a jour la BDD avec la valeur du client fourni
 */
	
	/*
	 * initiation variable
	 */

	// param sql
	$user_id = -1;
	/* Poste Benoit Principal */
	//$source = "mysql:host=localhost;dbname=slc_dev1";
	/* Poste Benoit Portable */
	$source = "mysql:host=localhost;dbname=test_presta";
	$user="root";
	$pass = "";
	/* ******************** */

	$user_id = $_GET['userId'];
	$supplier_id = $_GET['supplierId'];
	$employee_id = $_GET['employeeId'];

	$UPDATE_SUPPLIER = "UPDATE 
						ps_supplier
					SET 
						customer_id = " . $user_id . ", 
						employee_id = " . $employee_id . "
					WHERE 
						id_supplier = " . $supplier_id;

	// instanciation php data object
	$slc_db = new PDO($source, $user, $pass);

	// execution la requette SQL
	$slc_db->query($UPDATE_SUPPLIER);

 ?>