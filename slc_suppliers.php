<?php
/*
* 2015 LocalAddict
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Benoit MOTTIN <benaesan@msn.com>
*  @copyright  LocalAddict
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

// source http://nemops.com/prestashop-products-new-tabs-fields

if (!defined('_PS_VERSION_'))
	exit;

class slc_suppliers extends Module
{


	/**
	 * module's constructor / Required / grant to module to install himself
	 */
	public function __construct(){

		$this->name = 'slc_suppliers';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Benoit MOTTIN';
		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->bootstrap = true;
	
		parent::__construct();
	
		$this->displayName = $this->l('SurLeChamp Suppliers');
		$this->description = $this->l('ajout un lien entre le fournisseur et le client');
	
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
	
	}

	/**
	 * Show module's configuration
	 */
	public function getContent(){
		// //////////////TEST////////////////
	    
	}

    /**************************************************
     * 				INSTALLATION 					  *
     **************************************************/

	/**
	 * Execute certains traitement à l'installation du module
	 */
	public function install(){
			if (!parent::install() 
				|| !Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'supplier` ADD `employee_id` INT NULL, ADD `customer_id` INT NULL')
				//|| !Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'supplier` ADD `customer_id` INT NULL')
				//|| !Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'employee` ADD `customer_id` INT NULL')
				|| !$this->registerHook('displayAdminCustomerExtra')
				|| !$this->registerHook('displayBackOfficeHeader')
				) {
				return false;
			}
			return true;
	}

	/**
	 * Execute certains traitement à la désinstallation du module
	 */
	public function uninstall(){
		
    	if (!parent::uninstall() 
    			|| !Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'supplier` DROP `customer_id`, DROP `employee_id`')
    			
    			//|| !Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'supplier` DROP `customer_id`')
    			//|| !Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'employee` DROP `customer_id`')
    			){
				return false;
    		} 
		return true;
    	} 

	
	public function hookdisplayAdminCustomerExtra($params){
		/*if (Validate::isLoadedObject($customer = new Customer((int)Tools::getValue('id_customer')))){
			$this->prepareNewTab();
			return $this->display(__FILE__, 'testGeo.tpl');
		}//*/

	        //return $this->display(__FILE__, 'customer_supplier.tpl');
	    
	}

	public function hookDisplayBackOfficeHeader(){
		 $this->context->controller->addJS($this->_path.'js/function.js');
	}
}
