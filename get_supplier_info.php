<?php 
/*
* 2015 LocalAddict
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Benoit MOTTIN <benaesan@msn.com>
*  @copyright  LocalAddict
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

/**
 * Permet de recuperer l'enssemble des informations des producteurs, 
 * ainsi il sera possible de lier un client et un producteur
 */
	/*TODO 
	 * Ajout constrainte dans la requette pour ne pas afficher les fournisseurs existants
	 */
	
	/*
	 * initiation variable
	 */

	// param sql
	$user_id = -1;
	/* Poste Benoit Principal */
	//$source = "mysql:host=localhost;dbname=slc_dev1";
	/* Poste Benoit Portable */
	$source = "mysql:host=localhost;dbname=test_presta";
	
	$user="root";
	$pass = "";
	/* ******************** */

	// valeur client
	$sellers = array();

	/**
	 * Recuperation de tout les utilisateurs de type vendeur
	 */
	$SELECT_SELLERS = "SELECT 
							cu.id_customer as id,
							cu.firstname as firstname, 
							cu.lastname as lastname, 
							email, 
							phone, 
							address1, 
							postcode, 
							city, 
							iso_code
						FROM 
							ps_customer cu
						INNER JOIN 
							ps_address ad
						ON
							cu.id_customer = ad.id_customer
						INNER JOIN
							ps_country co
						ON 
							co.id_country = ad.id_country
						WHERE 
							cu.active = '1'
						AND
							cu.id_default_group = '4'
						";
							

	// instanciation php data object
	$slc_db = new PDO($source, $user, $pass);

	// execution la requette SQL + recuperation du resultat de celle-ci
	$res = $slc_db->query($SELECT_SELLERS);
	
	/*
	 * Recupere les infos utiles sur les vendeurs,
	 */

	while($ligne = $res->fetch()){
		$sellers[$ligne['id']] = array(
			'id' => $ligne['id'],
			'firstname'=> $ligne['firstname'],
			'lastname'=> $ligne['lastname'], 
			'phone'=> $ligne['phone'], 
			'email'=> $ligne['email'], 
			'address'=> $ligne['address1'], 
			'postcode'=> $ligne['postcode'], 
			'city'=> $ligne['city'], 
			'iso_code'=> $ligne['iso_code']);//*/
	}
	
	echo json_encode($sellers);

 ?>