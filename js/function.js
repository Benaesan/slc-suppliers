/*
* 2015 LocalAddict
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Benoit MOTTIN <benaesan@msn.com>
*  @copyright  LocalAddict
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

/**********ATTENTE DOCUMENT READY**************/
	$(document).ready(function(){
/**********************************************/
	/** ***********************************************************************************************
	 * PARAMETRAGE
	 ** **********************************************************************************************/
	//TODO
	// recuperer les ID du client et de l'employé pour mettre a jour les champs select generer
	
	// definition url suivant les postes
	// Benoit principal
	// var urlModule = "http://127.0.0.1/slc_dev/modules/slc_suppliers";
	
	// Benoit Portable
	var urlModule = "http://127.0.0.1/workspacePHP/test_prestashop/modules/slc_suppliers";
	
	//FIXME
	// rendre l'url dynamique
	
	/** ***********************************************************************************************
	 * Ajout client FICHE SUPPLIER
	 ** **********************************************************************************************/

	// Defintion des variables	
	
	if($("#supplier_form").length != 0){
		
		$champName = $("#name").parent().parent();
		
		// entete du champ fournisseur + label + style
		var labelSelectSellers = '	' + 
		'<div class="form-group" id="groupSelectSellers">' +

		'<label class="control-label col-lg-3"  title="Si le producteur n\'apparait pas, veuillez vérifier que le client possede au moins une adresse"> ' +
			'<span class="label-tooltip" data-toggle="tooltip" data-html="true">' +
				'Client' + 
			'</span>' +
		'</label>' +
		'<span id="slc_alert" class="warning control-label"></span>' + 
		'<div class="col-lg-3">';

		// requette ajax executer dès le chargement de la page
		$.ajax({
			url: urlModule+ '/get_supplier_info.php',
		})
		.done(function(json, statut) {
			listSellers = JSON.parse(String(json));
			selectSellers = "<select id='selectSellers'><option value='NULL'></option>";
			$.each(listSellers, function(index, el) {
				selectSellers += "<option value='" +
					 el.id + "'>" +
					 el.firstname + " " + 
					 el.lastname + "</option>";
			});
			selectSellers += "</select></div><div class='col-lg-1'><input id='btnMaj' type='button' value='MAJ'/></div></div>";
			$champName.after(labelSelectSellers + selectSellers);
			
			$("#selectSellers").on('change', function(){
				$sellerId = $("#selectSellers").val();
				// remplissage des differents champs obligatoire
				$("#name").val(listSellers[$sellerId].firstname + " " + listSellers[$sellerId].lastname);
				$("#address").val(listSellers[$sellerId].address);
				$("#postcode").val(listSellers[$sellerId].postcode);
				$("#city").val(listSellers[$sellerId].city);
			});

			$("#btnMaj").on('click', function(){
				// et on enregistre le changement de cle etrangere directement dans la base de donnée
				if($("#id_supplier").val()){
					$.ajax({
						url: urlModule + '/save_supplier.php',
						data: {userId: $("#selectSellers").val(), supplierId: $("#id_supplier").val(), employeeId: $("#selectEmployees").val()},
					})
					.done(function(res) {
						console.log("success");
					})
					.fail(function(error) {
						alert('error' + error);
						console.log("error");
					})
					.always(function() {
						console.log("complete");
					});//*/
				}else{
					$("#slc_alert").html("Pensez à sauvegarder le founisseur avant").show().fadeOut(4000);
				}
			});
		});//*/

	}

	/** **********************************************************************************************
	* Ajout employee FICHE SUPPLIER
	** **********************************************************************************************/

	if($("#supplier_form").length != 0){
		
	$champName = $("#name").parent().parent();
	
	// entete du champ fournisseur + label + style
	var labelSelectEmployees = '	' + 
	'<div class="form-group" id="groupSelectEmployees">' +
	'<label class="control-label col-lg-3"  title="Si le producteur n\'apparait, veuillez vérifier que le client possede au moins une adresse"> ' +
		'<span class="label-tooltip" data-toggle="tooltip" data-html="true">' +
			'Employée' + 
		'</span>' +
	'</label>' +
	'<span id="slc_alert" class="warning control-label"></span>' + 
	'<div class="col-lg-3">';

	// requette ajax executer dès le chargement de la page
	$.ajax({
		url: urlModule+ '/get_employee_info.php',
	})
	.done(function(json, statut) {
		listEmployees = JSON.parse(String(json));
		selectEmployees = "<select id='selectEmployees'><option value='NULL'></option>";
		$.each(listEmployees, function(index, el) {
			selectEmployees += "<option value='" +
				 el.id + "'>" +
				 el.firstname + " " + 
				 el.lastname + "</option>";
		});
		selectEmployees += "</select></div><div class='col-lg-1'></div>";
		$champName.after(labelSelectEmployees + selectEmployees);
		/*
		$("#selectEmployees").on('change', function(){
			$employeeId = $("#selectEmployees").val();
			// remplissage des differents champs obligatoire
			$("#name").val(listEmployees[$employeeId].firstname + " " + listSellers[$employeeId].lastname);
			$("#address").val(listEmployees[$employeeId].address);
			$("#postcode").val(listEmployees[$employeeId].postcode);
			$("#city").val(listEmployees[$employeeId].city);
		});
		*/
		/*$("#btnMaj").on('click', function(){
			// et on enregistre le changement de cle etrangere directement dans la base de donnée
			if($("#id_supplier").val()){
				$.ajax({
					url: urlModule + '/save_employee.php',
					data: {userId: $("#selectEmployees").val(), supplierId: $("#id_supplier").val()},
				})
				.done(function(res) {
					console.log("success");
				})
				.fail(function() {
					alert('error');
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});//*/
			/*}else{
				$("#slc_alert").html("Pensez à sauvegarder le founisseur avant").show().fadeOut(4000);
			}//*/
		//});
	});//*/

	}
	/** **********************************************************************************************
	 * MODIF FICHE EMPLOYEE OLD
	 ** **********************************************************************************************/
	/* if($("#employee_form") != 0){
		
	 // Definition des variables	
		$champLastname = $("#lastname").parent().parent();

		// entete du champ fournisseur + label + style
		var labelSelectSellers = '	' + 
		'<div class="form-group" id="groupSelectSellers">' +
		'<label class="control-label col-lg-3"  title="Si le producteur n\'apparait, veuillez vérifier que le client possede au moins une adresse"> ' +
			'<span class="label-tooltip" data-toggle="tooltip" data-html="true">' +
				'Producteur' + 
			'</span>' +
		'</label>' +
		'<span id="slc_alert" class="warning control-label"></span>' + 
		'<div class="col-lg-3">';

		// requette ajax executer dès le chargement de la page
		$.ajax({
			url: urlModule+ '/get_supplier_info.php',
		})
		.done(function(json, statut) {
			listSellers = JSON.parse(String(json));
			selectSellers = "<select id='selectSellers'><option value='NULL'></option>";
			$.each(listSellers, function(index, el) {
				selectSellers += "<option value='" +
					 el.id + "'>" +
					 el.firstname + " " + 
					 el.lastname + "</option>";
			});
			selectSellers += "</select></div><div class='col-lg-1'><input id='btnMaj' type='button' value='MAJ'/></div></div>";
			$champLastname.after(labelSelectSellers + selectSellers);

			$("#selectSellers").on('change', function(){
				$sellerId = $("#selectSellers").val();
				// remplissage des differents champs obligatoire
				$("#lastname").val(listSellers[$sellerId].lastname);
				$("#firstname").val(listSellers[$sellerId].firstname);
				$("#address").val(listSellers[$sellerId].address);
				$("#postcode").val(listSellers[$sellerId].postcode);
				$("#city").val(listSellers[$sellerId].city);
				$("#email").val(listSellers[$sellerId].email);
			});

			$("#btnMaj").on('click', function(){
				// et on enregistre le changement de cle etrangere directement dans la base de données
				if($("#id_employee").val()){
					$.ajax({
						url: urlModule + '/save_employee.php',
						data: {userId: $("#selectSellers").val(), employeeId: $("#id_employee").val()},
					})
					.done(function(res) {
						console.log("success");
					})
					.fail(function() {
						alert('error');
						console.log("error");
					})
					.always(function() {
						console.log("complete");
					});//*/
	/*			}else{
					$("#slc_alert").html("Pensez à sauvegarder l'employee avant").show().fadeOut(4000);
				}
			});
	});//*/


	
	//}
/**********FIN ATTENTE DOCUMENT READY**************/
	});
/**********************************************/

	